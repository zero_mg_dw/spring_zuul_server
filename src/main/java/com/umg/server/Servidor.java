/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umg.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author mmendez
 */
@SpringBootApplication
@EnableZuulProxy
public class Servidor {
    public static void main(String[] args) {
        SpringApplication.run(Servidor.class, args);
    }
}
